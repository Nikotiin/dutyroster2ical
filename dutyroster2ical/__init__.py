from .confluence_downloader import ConfluenceDownloader
from .html_parser import parse_document
from ics import Calendar, Event


class DutyCalendar:
    def __init__(self, confluence):
        self.confluence = confluence
        self.dates = None

    def update(self):
        self.dates = parse_document(self.confluence.get())

    def get(self, name):
        calendar = Calendar()

        try:
            for date in self.dates[name]:
                event = Event()
                event.name = "Työvuoro"
                event.begin = date['start']
                event.end = date['end']
                calendar.events.add(event)

        finally:
            return str(calendar)

    def get_all(self):
        calendar = Calendar()

        for officer, dates in self.dates.items():
            for date in dates:
                event = Event()
                event.name = "Työvuoro: " + officer
                event.begin = date['start']
                event.end = date['end']
                calendar.events.add(event)

        return str(calendar)
