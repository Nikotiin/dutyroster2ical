from urllib.parse import urlsplit
import requests


class ConfluenceDownloader:
    def __init__(self, url, username='', password='', auth_required=False):
        self.url = url

        parsed = urlsplit(url)
        # Get scheme and network location part of URL
        self.base_url = parsed[0] + '://' + parsed[1]
        # Get path and query parameters
        self.page_url = parsed[2] + '?' + parsed[3]

        self.session = requests.session()

        if auth_required:
            self.username = username
            self.password = password

    def get(self):
        response = self.session.get(self.url)

        if "login.action" in response.url:
            payload = {
                'os_username': self.username,
                'os_password': self.password,
                'login': 'Log in',
                'os_destination': self.page_url
            }
            response = self.session.post(self.base_url+'/dologin.action', data=payload)

        return response.content
