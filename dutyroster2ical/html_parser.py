from lxml import html
from datetime import datetime, timedelta
from itertools import chain
from pytz import timezone


def get_tables(html_string):
    tree = html.fromstring(html_string)
    return tree.find_class("confluenceTable")


def get_last_modified_year(html_string):
    tree = html.fromstring(html_string)
    last_modified = tree.find_class("last-modified")[0].text
    try:
        year = int(last_modified[-4:])
    except ValueError:
        year = datetime.now().year
    return year


def validate_table(table):
    rows = len(table.xpath("tbody/tr")) == 8
    columns = len(table.xpath("tbody/tr[2]/td")) >= 11

    return rows and columns


def group_by_officer(dates):
    date_map = dict()

    for date in dates:
        officer = date['person']
        cleaned = date
        cleaned.pop('person')

        if officer not in date_map:
            date_map[officer] = []
        date_map[officer].append(cleaned)

    return date_map


def parse_row(row, last_modified_year, week_one=False):
    columns = row.xpath("td")
    date = columns[1].text_content()
    night = columns[2].text_content()
    morning = columns[3].text_content()
    evening = columns[4].text_content()
    isit = columns[10].text_content()
    day, month = date.split('.')[:2]

    print(day)
    print(month)
    tz = timezone('Europe/Helsinki')

    # If we are at week one, put entries that are december to the current year, and other to upcoming year
    if (week_one == True and month == "12") or week_one == False:
        base_date = datetime(last_modified_year, int(month), int(day), 0, 0, 0)
    else:
        base_date = datetime(last_modified_year+1, int(month), int(day), 0, 0, 0)
    base_date = tz.localize(base_date)
    night_shift_start = base_date - timedelta(hours=1)
    morning_shift_start = base_date + timedelta(hours=7)
    isit_shift_start = base_date + timedelta(hours=9)
    evening_shift_start = base_date + timedelta(hours=15)
    night_shift_end = night_shift_start + timedelta(hours=8)
    morning_shift_end = morning_shift_start + timedelta(hours=8)
    isit_shift_end = isit_shift_start + timedelta(hours=8)
    evening_shift_end = evening_shift_start + timedelta(hours=8)

    duties = [{"start": night_shift_start,
              "end": night_shift_end,
              "person": night},
             {"start": morning_shift_start,
              "end": morning_shift_end,
              "person": morning},
             {"start": evening_shift_start,
              "end": evening_shift_end,
              "person": evening}]

    if isit != '':
        duties.append({"start": isit_shift_start,
                       "end": isit_shift_end,
                       "person": isit})

    return duties


def parse_table(table, last_modified_year):
    rows = table.xpath("tbody/tr")[1:]
    week_column = rows[0][0]
    week_number = int(week_column.text_content())
    week_column.getparent().remove(week_column)

    # Special handling for 1st week, as it may contain dates from previous year
    if week_number == 1:
        parsed = [parse_row(row, last_modified_year, week_one=True) for row in rows]
    else:
        parsed = [parse_row(row, last_modified_year) for row in rows]
    return list(chain.from_iterable(parsed)), week_number


def parse_document(html_string):
    tables = filter(validate_table, get_tables(html_string))
    year = get_last_modified_year(html_string)

    parsed = []
    previous_week_number = -1
    
    for table in tables:
        parsed_table, week_number = parse_table(table, year)
        parsed.append(parsed_table)
        # If week number is smaller than previous week, the year has changed.
        # Note that this will increment after week 1 has been already parsed
        # There is special handling for week one in parse_row()
        if week_number < previous_week_number:
            year += 1
        previous_week_number = week_number
        

    date_list = list(chain.from_iterable(parsed))
    return group_by_officer(date_list)
