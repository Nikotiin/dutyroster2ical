from pyramid.config import Configurator
from pyramid.response import Response
from waitress import serve
from threading import Thread
from datetime import datetime, timedelta
from time import sleep


class Webserver:
    def __init__(self, calendar, port):
        self.calendar = calendar
        self.port = port
        self.config = Configurator()

        self.config.add_route('duties', '/duties')
        self.config.add_view(self.duties, route_name="duties")
        self.app = self.config.make_wsgi_app()

        self.calendar_updated = datetime.now()
        self.updater_thread = Thread(target=self.__update_calendar__)
        self.updater_thread.start()

    def start(self):
        serve(self.app, host='0.0.0.0', port=self.port)

    def duties(self, request):
        name = request.params.get('name')
        content_type = "text/calendar"
        status_code = 200

        if name:
            data = self.calendar.get(name)
            if len(data) == 0:
                status_code = 404
        else:
            data = self.calendar.get_all()

        return Response(content_type=content_type, body=data, status=status_code)

    def __update_calendar__(self):
        while True:
            if (datetime.now() - self.calendar_updated) < timedelta(minutes=30):
                self.calendar.update()
                self.calendar_updated = datetime.now()
            sleep(60)
